from django.db import IntegrityError
from django.test import TestCase, Client
from django.urls import resolve
from .views import subscribe
from .models import DataSubscriber

# Create your tests here.
class SubscribePageUnitTest(TestCase):
    def test_url_subscribe_page_is_exist(self):
        response = Client().get('/subscribe/')
        self.assertEqual(response.status_code, 200)

    def test_subscribe_page_using_subscribe_function(self):
        found = resolve('/subscribe/')
        self.assertEqual(found.func, subscribe)

    def test_model_can_save_data_subscriber(self):
        DataSubscriber.objects.create(name="jihan",  email='jihan@gmail.com', password='1234')

        counting_all_input_data = DataSubscriber.objects.all().count()
        self.assertEqual(counting_all_input_data, 1)
    
    def test_self_func_name(self):
        DataSubscriber.objects.create(name='JIHANOCTA', email='jihan@gmail.com', password='1234')
        subscriber = DataSubscriber.objects.get(name='JIHANOCTA')
        self.assertEqual(str(subscriber), subscriber.name)

    def test_unique_email(self):
        DataSubscriber.objects.create(email="jihan@gmail.com")
        with self.assertRaises(IntegrityError):
            DataSubscriber.objects.create(email="jihan@gmail.com")

    def test_post_using_ajax(self):
        response = Client().post('/subscribe/post/', data={"name":"jihan", "email":"jihan113@gmail.com", "password":"123234",})
        self.assertEqual(response.status_code, 200)

    def test_check_email_view(self):
        response = Client().post('/subscribe/validasi/', data={"email":"jihaaaan@gmail.com"})
        self.assertEqual(response.status_code, 200)

    def test_check_email_already_exist(self):
        DataSubscriber.objects.create(name="jihan", email="jihan113@gmail.com", password="123234")
        response = Client().post('/subscribe/validasi/', data={"email": "jihan113@gmail.com"})
        self.assertEqual(response.json()['is_exists'], True)