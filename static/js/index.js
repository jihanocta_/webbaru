$( function() {
    $(window).on('load', function(){
        $('#status').delay(500).fadeOut();
        $('#preloader').delay(1000).fadeOut('slow');
    });

    $( "#accordion" ).accordion({
        collapsible: true,
        active: false
    });

    var local = window.localStorage;
    if(local.getItem('isPink') === 'true'){
        $("body").removeClass('body');
        $('.list p').removeClass('stat');
        $('.list p').addClass('stat-2');
        $('#change-theme').removeClass('change-theme');
        $('#change-theme').addClass('change-theme-2');
        $('#btn-profile').removeClass('btn-profile');
        $('#btn-profile').addClass('btn-profile-2');
        $('#submit').removeClass('submit');
        $('#submit').addClass('submit-2');
        $('#id_isi_status').removeClass('form-control');
        $('#id_isi_status').addClass('form-control-2');
        $('#books-collection').removeClass('books-collection');
        $('#books-collection').addClass('books-collection-2');
    }
    else{
        $("body").addClass("body");
        $('.list p').addClass('stat');
        $('.list p').removeClass('stat-2');
        $('#change-theme').addClass('change-theme');
        $('#change-theme').removeClass('change-theme-2');
        $('#btn-profile').addClass('btn-profile');
        $('#btn-profile').removeClass('btn-profile-2');
        $('#submit').addClass('submit');
        $('#submit').removeClass('submit-2');
        $('#id_isi_status').addClass('form-control');
        $('#id_isi_status').removeClass('form-control-2');
        $('#books-collection').addClass('books-collection');
        $('#books-collection').removeClass('books-collection-2');
    }
    document.getElementById('change-theme').onclick = changeTheme;
    function changeTheme() {
        if ($("body").hasClass('body')) {
            $("body").removeClass('body');
            $('.list p').removeClass('stat');
            $('.list p').addClass('stat-2');
            $('#change-theme').removeClass('change-theme');
            $('#change-theme').addClass('change-theme-2');
            $('#btn-profile').removeClass('btn-profile');
            $('#btn-profile').addClass('btn-profile-2');
            $('#submit').removeClass('submit');
            $('#submit').addClass('submit-2');
            $('#id_isi_status').removeClass('form-control');
            $('#id_isi_status').addClass('form-control-2');
            $('#books-collection').removeClass('books-collection');
            $('#books-collection').addClass('books-collection-2');
            local.setItem('isPink', true);
        }
        else {
            $("body").addClass("body");
            $('.list p').addClass('stat');
            $('.list p').removeClass('stat-2');
            $('#change-theme').addClass('change-theme');
            $('#change-theme').removeClass('change-theme-2');
            $('#btn-profile').addClass('btn-profile');
            $('#btn-profile').removeClass('btn-profile-2');
            $('#submit').addClass('submit');
            $('#submit').removeClass('submit-2');
            $('#id_isi_status').addClass('form-control');
            $('#id_isi_status').removeClass('form-control-2');
            $('#books-collection').addClass('books-collection');
            $('#books-collection').removeClass('books-collection-2');
            local.setItem('isPink', false);
        }
    }
});