from importlib import import_module

import requests
from django.conf import settings
from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.http import request
from .views import index

# Create your tests here.


class SessionTestCase(TestCase):
    def setUp(self):
        settings.SESSION_ENGINE = "django.contrib.sessions.backends.file"
        engine = import_module(settings.SESSION_ENGINE)
        store = engine.SessionStore()
        store.save()
        self.session = store
        self.client.cookies[settings.SESSION_COOKIE_NAME] = store.session_key


class TestCaseStory9(SessionTestCase):
    def test_url_not_login_books_page_redirect_to_other_page(self):
        response = Client().get(reverse('books_page'))
        self.assertEqual(response.status_code, 302)

    def test_url_login_books_page(self):
        test = self.client.session
        test['user_id'] = 'jihan'
        test['name'] = 'jihan'
        test.save()

        response = self.client.get(reverse('books_page'))
        self.assertEqual(response.status_code, 200)

    def test_books_page_using_index_function(self):
        found = resolve(reverse('books_page'))
        self.assertEqual(found.func, index)

    def test_client_can_post_and_render(self):
        test = self.client.session
        test['user_id'] = 'jihann'
        test['name'] = 'jihan'
        test.save()

        response_post = self.client.post('/book/add/', {'id': 'xyz'})
        self.assertEqual(response_post.status_code, 200)

    # def test_client_can_delete_and_delete_selected(self):
    #     title = "omaigat"
    #     test = self.client.session
    #     test['user_id'] = 'jihan'
    #     test['name'] = 'jihan'
    #     test['books'] = [judul]
    #     test.save()

    #     response_post = self.client.post('/book/remove/', {'id': title})
    #     self.assertEqual(response_post.status_code, 200)

    # def test_client_can_get_data(self):
    #     test = self.client.session
    #     test['user_id'] = 'pooh'
    #     test['name'] = 'twinky'
    #     test['books'] = []
    #     test.save()

    #     response = self.client.get('/book/render/')
    #     self.assertEqual(response.status_code, 200)

    # def test_books_not_favorited(self):
    #     test = self.client.session
    #     test['user_id'] = 'octa'
    #     test['name'] = 'maulana'
    #     test['books'] = []
    #     test.save()

    #     response = self.client.get('/book/render/').json()
    #     self.assertFalse(response['data'][0]['boolean'])
