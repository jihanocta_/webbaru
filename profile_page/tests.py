from django.test import TestCase
from django.test import Client
from django.urls import resolve, reverse
from .views import profile

# Create your tests here.
class Lab6ChallangeUnitTest(TestCase):

    def test_lab6_url_is_exist(self):
        response = Client().get(reverse('profile_page'))
        self.assertEqual(response.status_code, 200)

    def test_lab6_using_index_func(self):
        found = resolve(reverse('profile_page'))
        self.assertEqual(found.func, profile)
    
    def test_profile_page_contains_name(self):
        response = Client().get(reverse('profile_page'))
        html_response = response.content.decode('utf8')
        self.assertIn('Jihan Maulana Octa', html_response)
    
    def test_profile_page_contains_npm(self):
        response = Client().get(reverse('profile_page'))
        html_response = response.content.decode('utf8')
        self.assertIn('1706984644', html_response)

    def test_profile_page_contains_university(self):
        response = Client().get(reverse('profile_page'))
        html_response = response.content.decode('utf8')
        self.assertIn('University of Indonesia', html_response)