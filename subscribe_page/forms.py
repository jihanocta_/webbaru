from django import forms
from .models import DataSubscriber

class FormSubscribe(forms.ModelForm):
    class Meta:
        model = DataSubscriber
        fields = '__all__'
        
    name = forms.CharField(label = "NAME", required=True, max_length=100, widget=forms.TextInput(attrs={'id': 'name'}))
    email = forms.CharField(label = 'EMAIL:', required=True, max_length=100, widget=forms.EmailInput(attrs={'id': 'email'}))
    password = forms.CharField(label = 'PASSWORD:', required=True, max_length=50, widget=forms.PasswordInput(attrs={'id': 'password'}))