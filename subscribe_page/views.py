from django.http import JsonResponse
from django.shortcuts import render
from .forms import FormSubscribe
from .models import DataSubscriber

# Create your views here.

def subscribe(request):
    return render(request, 'subscribe.html', {'form': FormSubscribe})

def emailValidate(request):
    if request.method == 'POST':
        email = request.POST['email']
        check = DataSubscriber.objects.filter(email=email)
        if check.exists():
            return JsonResponse({'is_exists': True})
        return JsonResponse({'is_exists': False})

def save_to_model(request):
    if request.method == 'POST':
        name = request.POST['name']
        email = request.POST['email']
        password = request.POST['password']
        DataSubscriber.objects.create(name=name, email=email, password=password)
        return JsonResponse({'is_success': True})

def data_subscribe(request):
    if request.method == 'POST':
        dataSubscriber = DataSubscriber.objects.all().values()
        listData = list(dataSubscriber)
        return JsonResponse({'listData': listData})

def result(request):
    return render(request, 'challange.html')

def delete_from_model(request):
    if request.method == 'POST':
        email = request.POST['email']
        password = request.POST['password']
        data = DataSubscriber.objects.filter(email=email, password=password)
        if data.exists():
            data.delete()
            return JsonResponse({'deleted': True})
        return JsonResponse({'deleted': False})
