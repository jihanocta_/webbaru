"""project_baru URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from landing_page.views import index
from profile_page.views import profile

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', index, name="landing_page"),
    path('profile/', profile, name="profile_page"),
    path('book/', include('books_page.urls')),
    path('subscribe/', include('subscribe_page.urls')), 
    path('login/', include('login_page.urls')),  
]
