from django.urls import path
from .views import *

urlpatterns = [
    path('', subscribe, name="subscribe"),
    path('validasi/', emailValidate, name="validasi"),
    path('post/', save_to_model, name="post"),
    path('subs/', data_subscribe, name="dataSubscribe"),
    path('dataSubscribe/', result, name="result"),
    path('delete/', delete_from_model, name="delete"),
]