from django.test import TestCase
from django.test import Client

# Create your tests here.


class LoginUnitTest(TestCase):
    def test_books_page_not_logged_landing_url_is_exist(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_story11_false_token(self):
        response = Client().post('/login/', {'id_token': "test"}).json()
        self.assertEqual(response['status'], "1")

    def test_story11_logout(self):
        response = Client().get('/login/logout/')
        self.assertEqual(response.status_code, 302)
