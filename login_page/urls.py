from django.urls import path
from .views import *

urlpatterns = [
    path('', login, name="login-page"),
    path('logout/', logout, name="logout")
]