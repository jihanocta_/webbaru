import time
from django.test import TestCase
from django.test import Client
from django.urls import resolve, reverse
from .views import index
from .models import PostForms
from .forms import Post_Forms
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class Lab6UnitTest(TestCase):

    def test_lab6_url_is_exist(self):
        response = Client().get(reverse('landing_page'))
        self.assertEqual(response.status_code, 200)

    def test_lab6_using_index_func(self):
        found = resolve(reverse('landing_page'))
        self.assertEqual(found.func, index)
    
    def test_model_can_create_new_todo(self):
        new_post = PostForms.objects.create(isi_status='status')
        counting_all_available_post = PostForms.objects.all().count()
        self.assertEqual(counting_all_available_post, 1)

    def test_form_validation_for_blank_items(self):
        form = Post_Forms(data={'isi_status': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['isi_status'],
            ["This field is required."]
        )

    def test_lab6_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post(reverse('landing_page'), {'isi_status': test})
        self.assertEqual(response_post.status_code, 302)

        response = Client().get(reverse('landing_page'))
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_lab6_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post(reverse('landing_page'), {'isi_status': ''})
        self.assertNotEqual(response_post.status_code, 302)

        response = Client().get(reverse('landing_page'))
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)
    
class LandingPageFunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(LandingPageFunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(LandingPageFunctionalTest, self).tearDown()

    def test_input_todo(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://web-status1.herokuapp.com/')
        # Find the form element
        new_post = selenium.find_element_by_name("isi_status")
        submit = selenium.find_element_by_id('submit')
        time.sleep(5)

        # Fill the form with data
        new_post.send_keys('Coba Coba')

        # Submitting the form
        submit.send_keys(Keys.RETURN)

        # self.assertIn("Status!", selenium.title)
        self.assertIn("Coba Coba", selenium.page_source)

    def test_title_header_layout(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://web-status1.herokuapp.com/')
        # Find the form element
        title = selenium.find_element_by_id("title").text
        head = selenium.find_element_by_id("head").text
        time.sleep(5)

        self.assertIn(title, head)

    def test_title_body_layout(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://web-status1.herokuapp.com/')
        # Find the form element
        hello = selenium.find_element_by_id("hello").text
        body = selenium.find_element_by_id("body").text
        time.sleep(5)

        self.assertIn(hello,body)

    def test_style_element(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://web-status1.herokuapp.com/')
        # Find the form element
        submit = selenium.find_element_by_id("submit").value_of_css_property("background-color")
        
        self.assertEqual(submit, "rgba(143, 188, 143, 1)")
        
    def test_font_color_element(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://web-status1.herokuapp.com/')
        # Find the form element
        hello = selenium.find_element_by_id("hello").value_of_css_property("color")
        
        self.assertEqual(hello, "rgba(0, 0, 0, 1)")

    # def test_color_change(self):
    #     selenium =self.selenium
    #     # Opening the link we want to test
    #     selenium.get('http://web-status1.herokuapp.com/')
    #     # Find the form element
    #     button = self.selenium.find_element_by_id('change-theme')
    #     button.click()
    #     list = self.selenium.find_element_by_class_name('stat-2')
    #     color = list.value_of_css_property('background-color')
    #     self.assertEqual(color, 'rgba(233, 150, 122, 1)')
