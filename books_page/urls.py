from django.urls import path
from .views import *

urlpatterns = [
    path('', index, name="books_page"),
    path('add/', add_book_to_favorited, name="add-favorites"),
    path('remove/', remove_book_favorited, name="remove-favorites"),
    path('render/', get_data, name="get-data"),

]