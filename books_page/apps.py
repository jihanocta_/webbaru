from django.apps import AppConfig


class BooksPageConfig(AppConfig):
    name = 'books_page'
