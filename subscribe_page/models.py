from django.db import models

# Create your models here.
class DataSubscriber(models.Model):
    name = models.CharField(max_length=100)
    email = models.CharField(max_length=100, primary_key=True, unique=True)
    password = models.CharField(max_length=50)

    def __str__(self):
        return self.name