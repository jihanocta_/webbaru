from django.shortcuts import render
from .forms import Post_Forms
from .models import PostForms

from django.http import HttpResponseRedirect

# Create your views here.
def index(request):
    if request.method == 'POST':
        new_post = Post_Forms(request.POST)
        if new_post.is_valid():
            data = new_post.cleaned_data
            PostForms.objects.create(**data)
            return HttpResponseRedirect('/')
    else:
        new_post = Post_Forms()
    result = list(PostForms.objects.all())
    result.reverse()
    content = {'status': result, 'form' : new_post}
    return render(request, 'index.html', content)
